﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelPickManager : MonoBehaviour
{

    public Button Level1;
    public Button Level2;
    public Button Level3;
    public Button Level4;
    public Button Level5;
    public Button Level6;
    public Button Level7;
    public Button Level8;
    public Button Level9;
    public Button Level10;

    // Use this for initialization
    void Start()
    {
        Level1.onClick.AddListener(onClickLevel1Button);
        Level2.onClick.AddListener(onClickLevel2Button);
        Level3.onClick.AddListener(onClickLevel3Button);
        Level4.onClick.AddListener(onClickLevel4Button);
        Level5.onClick.AddListener(onClickLevel5Button);
        Level6.onClick.AddListener(onClickLevel6Button);
        Level7.onClick.AddListener(onClickLevel7Button);
        Level8.onClick.AddListener(onClickLevel8Button);
        Level9.onClick.AddListener(onClickLevel9Button);
        Level10.onClick.AddListener(onClickLevel10Button);

    }

    // Update is called once per frame
    void Update()
    {

    }

    void onClickLevel1Button()
    {
        SceneManager.LoadScene("Gameplay");
    }

    void onClickLevel2Button()
    {
        SceneManager.LoadScene("Stage 2");
    }

    void onClickLevel3Button()
    {
        SceneManager.LoadScene("Stage 3");
    }

    void onClickLevel4Button()
    {
        SceneManager.LoadScene("Stage 4");
    }

    void onClickLevel5Button()
    {
        SceneManager.LoadScene("Stage 5");
    }

    void onClickLevel6Button()
    {
        SceneManager.LoadScene("Stage 6");
    }

    void onClickLevel7Button()
    {
        SceneManager.LoadScene("Stage 7");
    }

    void onClickLevel8Button()
    {
        SceneManager.LoadScene("Stage 8");
    }

    void onClickLevel9Button()
    {
        SceneManager.LoadScene("Stage 9");
    }

    void onClickLevel10Button()
    {
        SceneManager.LoadScene("Stage 10");
    }
}
