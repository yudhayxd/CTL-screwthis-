﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public enum EnemyState
    {
        Die,
        Approach,
        Wander,
        RegenHP,
        Retreat
    }

    public EnemyState currentState;
    private Player player;
    [SerializeField] EnemySprite[] EnemySprite;

    public float maxHealth;
    public float currentHealth;
    public float speed;
    public float damage;
    public float delayDeathTime;

    float enemyHitTimer;

    private float DistanceToPlayer;

    private const float SPEED_STANDARD = 0.03f;

    private const float DISTANCE_TO_PLAYER_THRESHOLD = 5;

    private bool isCompleteMove;
    Vector3 randomGotoPos;

    Animator anim;

    private void Start()
    {
        currentState = EnemyState.Wander;
        currentHealth = maxHealth;
        player = FindObjectOfType<Player>();
        anim = GetComponent<Animator>();
    }

    public void Approach()
    {
        // Debug.Log("Enemy approaching");
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, SPEED_STANDARD * speed);
        if (DistanceToPlayer > DISTANCE_TO_PLAYER_THRESHOLD)
        {
            currentState = EnemyState.Wander;
        }
        if (DistanceToPlayer < DISTANCE_TO_PLAYER_THRESHOLD && currentHealth < maxHealth / 2)
        {
            currentState = EnemyState.Retreat;
        }
        if (currentHealth <= 0)
        {
            currentState = EnemyState.Die;
        }
    }

    // its me aisy
    // Hello there... general aisy
    // damn so scare..

    public void Wander()
    {
        //Debug.Log("Enemy wandering");
        if (!isCompleteMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, randomGotoPos, SPEED_STANDARD * speed / 2);
        }
        if (Vector3.Distance(transform.position, randomGotoPos) <= 1)
        {
            isCompleteMove = true;
            MoveToNextPointWander();
        }
        if (DistanceToPlayer < DISTANCE_TO_PLAYER_THRESHOLD && currentHealth >= maxHealth / 2)
        {
            currentState = EnemyState.Approach;
        }
        if (currentHealth < maxHealth / 2)
        {
            currentState = EnemyState.RegenHP;
        }
        if (currentHealth <= 0)
        {
            currentState = EnemyState.Die;
        }
    }

    public void RegenHP()
    {
        Debug.Log("Enemy Regenerating HP");
        anim.SetBool("regen", true);
        currentHealth += 0.1f;
        if (DistanceToPlayer < DISTANCE_TO_PLAYER_THRESHOLD && currentHealth >= maxHealth / 2)
        {
            anim.SetBool("regen", false);
            currentState = EnemyState.Approach;
        }
        else if (DistanceToPlayer < DISTANCE_TO_PLAYER_THRESHOLD && currentHealth < maxHealth / 2)
        {
            anim.SetBool("regen", false);
            currentState = EnemyState.Retreat;
        }
        if (currentHealth >= maxHealth)
        {
            anim.SetBool("regen", false);
            currentState = EnemyState.Wander;
        }
        if (currentHealth <= 0)
        {
            anim.SetBool("regen", false);
            currentState = EnemyState.Die;
        }
    }

    public void Retreat()
    {
        Debug.Log("Enemy Retreat");
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, -SPEED_STANDARD * speed);
        if (DistanceToPlayer > DISTANCE_TO_PLAYER_THRESHOLD)
        {
            currentState = EnemyState.Wander;
        }
        if (currentHealth <= 0)
        {
            currentState = EnemyState.Die;
        }
    }

    public void Die()
    {

        Debug.Log("Died!");
        delayDeathTime -= Time.deltaTime;
        if (delayDeathTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        switch (currentState)
        {
            case EnemyState.Approach: Approach(); break;
            case EnemyState.Wander: Wander(); break;
            case EnemyState.RegenHP: RegenHP(); break;
            case EnemyState.Retreat: Retreat(); break;
            case EnemyState.Die: Die(); break;
        }

        DistanceToPlayer = Vector3.Distance(gameObject.transform.position, player.gameObject.transform.position);
        //Debug.Log("DistanceToPlayer" + DistanceToPlayer);
        enemyHitTimer -= Time.deltaTime;
        if (enemyHitTimer < 0)
        {
            anim.SetBool("hit", false);
        }
    }

    private void MoveToNextPointWander()
    {
        randomGotoPos = new Vector3(Random.Range(transform.position.x - 3, transform.position.x + 3),
                                            Random.Range(transform.position.y - 3, transform.position.y + 3));
        isCompleteMove = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            Player player = collision.GetComponent<Player>();
            enemyHitTimer = 0.5f;
            if (!player.isDashing)
            {
                player.healthPoint -= damage;
            }
            if (player.isDashing)
            {
                anim.SetBool("hit", true);
            }
        }
    }
}
