﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class EnemySprite
{

    public Sprite walkSprite;
    public Sprite regenSprite;
    public Sprite hitSprite;

}
