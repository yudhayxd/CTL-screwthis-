﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{

    public Button playButton;
    public Button encyclopediaButton;
    public Button levelPickButton;
    public Button helpButton;

    // Use this for initialization
    void Start()
    {
        playButton.onClick.AddListener(onClickPlayButton);
        encyclopediaButton.onClick.AddListener(onClickEncyclopediaButton);
        levelPickButton.onClick.AddListener(onClickLevelPickButton);
        helpButton.onClick.AddListener(onClickHelpButton);
    }

    // Update is called once per frame
    void Update()
    {

    }
    void onClickPlayButton()
    {
        SceneManager.LoadScene("MainMenu");

    }
    void onClickEncyclopediaButton()
    {
        SceneManager.LoadScene("Encyclopedia");

    }
    void onClickLevelPickButton()
    {
        SceneManager.LoadScene("LevelPick");
    }
    void onClickHelpButton()
    {
        SceneManager.LoadScene("Help");

    }
}
