﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    public Player player;
    public Enemy[] enemy;
    public float globalTimer;
    public float cameraSpeed;
    public float distanceBetweenPlayerAndCamera;
    public float level;

    void Start()
    {
        GameplayDataManager.getInstance().currentLevel = 1;
        Debug.Log("Current level " + GameplayDataManager.getInstance().currentLevel);
        GeneratePlayer();
        player = FindObjectOfType<Player>();
        if (level<6)
        {
            for (int i = 0; i < enemy.Length; i++)
            {
                Instantiate(enemy[i], new Vector3(Random.Range(0f, 28.0f), Random.Range(-5.0f, 5.0f), 0), Quaternion.identity);
            }
        }
        if(level>5)
        {
            for (int i = 0; i < enemy.Length; i++)
            {
                Instantiate(enemy[i], new Vector3(Random.Range(0f, 67.0f), Random.Range(-5.0f, 5.0f), 0), Quaternion.identity);
            }
        }
        
    }

    void Update()
    {
        globalTimer -= Time.deltaTime;
        float cameraHeight = Camera.main.orthographicSize;
        float cameraWidth = cameraHeight * Camera.main.aspect;
        float positionOfRightSideCamera = Camera.main.transform.position.x + cameraWidth;
        float positionOfLeftSideCamera = Camera.main.transform.position.x - cameraWidth;
        if (Vector3.Distance(new Vector3(player.transform.position.x, 0, 0),
                            new Vector3(positionOfRightSideCamera, 0, 0)) < distanceBetweenPlayerAndCamera)
        {
            Camera.main.transform.position += new Vector3(Time.deltaTime * cameraSpeed, 0, 0);
        }
        if (Vector3.Distance(new Vector3(player.transform.position.x, 0, 0),
                            new Vector3(positionOfLeftSideCamera, 0, 0)) < distanceBetweenPlayerAndCamera)
        {
            Camera.main.transform.position -= new Vector3(Time.deltaTime * cameraSpeed, 0, 0);
        }
        //if(player.){
        //
        //}
    }

    private void GeneratePlayer()
    {
        Instantiate(player, new Vector3(-7, 0, 0), Quaternion.identity);
    }

    

}
