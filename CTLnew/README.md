# Feature List

Cocy The Leucocytes Features
Diagrams:
https://www.draw.io/#WB59F2C889083C14C!13814

## Basic Functions/Features

1.  **Main Menu**
    - Buttons link
    - Buttons Sounds
    - BGM

1.  **Tutorial and How to**
    - Tutorial
    - Encyclopedia


## Gameplay

1.  **Core Gameplay**

    - Enemies State Sprites
    - Player State Sprites :done:
    - Player State Sounds
    - Enemies State Sounds
    - Enemies physics
    - Red Blood NPC (spawn random on map only as colliding object with no AI)
    - Level Design
    - HUD (Needs Fix)
    - Pop Ups
    - BGM (not available yet)
    - Starting Cutscene
    - Ending Cutscene

2.  **Controller**

    - Joystick :done:
    - Dash Button :done:

3.  **AI**
    - Enemy States :done:
    - Player States :done:

4.  **Level**

    - Level 1
        - Description: 
        - Main Character  : Monocytes
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Germs. 
        - Enemies Count	: 10
        - Goal 		    : Kill all

    - Level 2
        - Description: 
        - Main Character  : Monocytes
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Bacteria & Germs. 
        - Enemies Count	: 20
        - Goal 		    : Kill all
    - Level 3
        - Description: 
        - Main Character  : Lymphocytes
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Fungus & Germs.
        - Enemies Count	: 20
        - Goal 		    : Kill all

    - Level 4
        - Description: 
        - Main Character  : Lymphocytes
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Fungus & Germs.
        - Enemies Count	: 30
        - Goal		    : Kill all

    - Level 5
        - Description: 
        - Main Character  : Neutrophils
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Parasites & Germs.
        - Enemies Count	: 30
        - Goal 		    : Kill all


    - Level 6
        - Description: 
        - Main Character  : Neutrophils
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Parasites & Germs.
        - Enemies Count	: 40
        - Goal 		    : Kill all

    - Level 7
        - Description: 
        - Main Character  : Basophils
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Parasites, Fungus & Germs.
        - Enemies Count	: 50
        - Goal		    : Kill all

    - Level 8
        - Description: 
        - Main Character  : Basophils
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Parasites, Fungus & Germs.
        - Enemies Count	: 60
        - Goal 		    : Kill all


    - Level 9
        - Description: 
        - Main Character  : Eosinophils
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Cancer, Parasites & Germs.
        - Enemies Count	: 70
        - Goal 		    : Kill all

    - Level 10
        - Description: 
        - Main Character  : Eosinophils
        - Elements/map	: Microscopic blood veins, Veins wall 
        - Enemies 	    : Cancer, Parasites & Germs.
        - Enemies Count	: 80
        - Goal 		    : Kill all

